﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using YFW;

public class ReadyPrefabController : MonoBehaviour
{
    // メンバ変数定義
    Text mShapeType;
    Text mColorType;
    Transform mBG;
    Transform mStartReady;
    Transform mBestScore;
    Text mHiscoreTime;
    Text mHiscoreRotate;
    Text mHiscoreChange;
    Button mOK;

    Text mCountDown;

    private readonly string ShapeFormat = "Shape_{0:00}";
    private readonly string ColorFormat = "Color_{0}";
        

    private void Awake()
    {
        // UI参照リンク構築
        mBG = transform.FindChild("StartMask");
        mStartReady = mBG.FindChild("StageReady");
        mShapeType = mStartReady.FindChild("Shape/Shape_val").GetComponent<Text>();
        mColorType = mStartReady.FindChild("Color/Color_val").GetComponent<Text>();
        mOK = mStartReady.FindChild("OK").GetComponent<Button>();
        mCountDown = mBG.FindChild("CountDown").GetComponent<Text>();

        mBestScore = mStartReady.FindChild("BestScore");
        mHiscoreTime = mBestScore.FindChild("Time/Time_val").GetComponent<Text>();
        mHiscoreRotate = mBestScore.FindChild("Rotate/Rotate_val").GetComponent<Text>();
        mHiscoreChange = mBestScore.FindChild("Change/Change_val").GetComponent<Text>();

        mBestScore.gameObject.SetActive(false);
    }

    private void Start()
    {
        // 相手に参照できるよう渡す
        GameSystem.Instance.mReadyCont = this;
    }

    /// <summary>
    /// ゲーム選択情報を入力
    /// </summary>
    public void setParameter( int shapenum, int colornum )
    {
        if (LocalizeManager.Instance)
        {
            mShapeType.text = LocalizeManager.Instance.getContents("Category_Data", string.Format(ShapeFormat,shapenum));
            mColorType.text = LocalizeManager.Instance.getContents("Category_Data", string.Format(ColorFormat, colornum));

            if (SaveManager.Instance.configSave)
            {
                ConfigSaveData.StageProgress progress = null;
                SaveManager.Instance.configSave.getProgressSaveData(ref progress, AppDefine.getStageID(AppMain.Instance.stageInfo._shapeNum, AppMain.Instance.stageInfo._colorNum));
                if (progress != null && (progress._best_time > -1))
                {
                    mBestScore.gameObject.SetActive(true);
                    mHiscoreTime.text = progress._best_time.ToString();
                    mHiscoreRotate.text = progress._best_rotateNum.ToString();
                    mHiscoreChange.text = progress._best_changeNum.ToString();
                }
            }
        }
    }

    /// <summary>
    /// 準備完了ではじめるボタンを表示
    /// </summary>
    public void activateAction()
    {
        //　データ入力が許可されたらtrue
        mOK.gameObject.SetActive(true);

        if( SaveManager.Instance.configSave.mPreferenceInfo._tutorialStep == (int)ConfigSaveData.eTutorialStep.NEW)
        {
            GameSystem.Instance.openTutorial();
        }
    }

    /// <summary>
    /// 開始ボタンが押された時のアクション
    /// </summary>
    public void PushButton()
    {
        // 情報を閉じる.
        mStartReady.gameObject.SetActive(false);
        // カウントダウン表示
        mCountDown.gameObject.SetActive(true);
        // レディ中ステップに移行
        GameSystem.Instance.mNextGameMainStep = GameSystem.eGameStep.Ready;
        if (AudioManager.Instance) { AudioManager.Instance.StopBGM(); }

        // 現在のステージのステータス更新
        ConfigSaveData.StageProgress progress = null;
        SaveManager.Instance.configSave.getProgressSaveData(ref progress, AppDefine.getStageID(AppMain.Instance.stageInfo._shapeNum, AppMain.Instance.stageInfo._colorNum));
        if (progress != null)
        {
            // まだ未プレイのときのみ更新
            if (progress._progressNum == ConfigSaveData.eStageProgress.NEW)
            {
                progress._progressNum = ConfigSaveData.eStageProgress.PLAYED;
                SaveManager.Instance.SaveAll();
            }
        }
        StartCoroutine(_readyAction());
    }

    /// <summary>
    /// ステージセレクトへ戻るが押された時のアクション
    /// </summary>
    public void BackToStageSelect()
    {
        if( AppMain.Instance)
        {
            AppMain.SE_OK();
            AppMain.Instance.BackToStageSelect();
        }
    }

    private IEnumerator _readyAction()
    {
        int timer = AppDefine.ReadyCountNum;
        var wait = new WaitForSeconds(1f);
        while (timer > 0)
        {
            mCountDown.text = timer.ToString();
            timer--;
            if (AudioManager.Instance) { AudioManager.Instance.PlaySe("se_do"); }
            yield return wait;  // 1秒待機
        }

        if (AudioManager.Instance) { AudioManager.Instance.PlaySe("se_shan"); }
        if (AudioManager.Instance) { AudioManager.Instance.PlayBGM("bgm"); }
        this.gameObject.SetActive(false);
        // ゲーム中ステップに移行
        GameSystem.Instance.mNextGameMainStep = GameSystem.eGameStep.Ingame;
    }

    /// <summary>
    /// Ready Prefabを起動
    /// </summary>
    public void resetReadyPrefab()
    {
        // 情報を閉じる.
        mStartReady.gameObject.SetActive(true);
        mCountDown.gameObject.SetActive(false);
        this.gameObject.SetActive(true);
    }
#if false
    private Transform getChildTransform(string name, Transform parent)
    {
        Transform t = null;
        if (parent == null) return null;
        var vals = parent.GetComponentsInChildren<Transform>();
        if (vals != null)
        {
            foreach (var tmp in vals)
            {
                if (tmp.name.Equals(name))
                {
                    t = tmp;
                    break;
                }
            }
        }
        return t;
    }
#endif
}
