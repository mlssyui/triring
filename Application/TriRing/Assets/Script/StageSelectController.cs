﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using YFW;

public class StageSelectController : MonoBehaviour
{
    AppDefine.eShapeNum m_ShapeNum = AppDefine.eShapeNum.HEXAGON;
    int                 m_ColorNum = 3;
    bool                m_Random = false;
    
    Toggle  m_Randomize;

    bool m_Activate = false;

    Dictionary<AppDefine.eShapeNum, Toggle>             m_ShapeToggleDict   = new Dictionary<AppDefine.eShapeNum, Toggle>();
    Dictionary<AppDefine.eShapeNum, ProgressController> m_ShapeProgressDict = new Dictionary<AppDefine.eShapeNum, ProgressController>();
    Dictionary<int, Toggle>                             m_ColorToggleDict   = new Dictionary<int, Toggle>();
    Dictionary<int, ProgressController>                 m_ColorProgressDict = new Dictionary<int, ProgressController>();

    /// <summary>
    /// 保存辞書のデータクラス
    /// </summary>
    class ProgressInShape {
        public ConfigSaveData.eStageProgress _shapeProgress;
        public Dictionary<int, ConfigSaveData.eStageProgress> _progressDict;
    }
    /// <summary>
    /// 全体のプログレス情報を一時保存する辞書
    /// </summary>
    Dictionary<AppDefine.eShapeNum, ProgressInShape> m_ProgressInfoDict = new Dictionary<AppDefine.eShapeNum, ProgressInShape>();
    
    /// <summary>
    /// コンポーネントを適用する
    /// </summary>
    /// <param name="shapenum"></param>
    /// <param name="trn"></param>
    private void setShapeComponents(AppDefine.eShapeNum shapenum, Transform trn)
    {
        if (trn == null)
        {
            Debug.LogErrorFormat("指定したTransformが存在しない = {0}",shapenum);
            return;
        }
        m_ShapeToggleDict.Add(shapenum, trn.GetComponent<Toggle>());
        m_ShapeProgressDict.Add(shapenum, trn.GetComponent<ProgressController>());
    }

    /// <summary>
    /// コンポーネントを適用する
    /// </summary>
    /// <param name="colornum"></param>
    /// <param name="trn"></param>
    private void setColorComponents(int colornum, Transform trn)
    {
        if (trn == null)
        {
            Debug.LogErrorFormat("指定したTransformが存在しない = {0}", colornum);
            return;
        }
        m_ColorToggleDict.Add(colornum, trn.GetComponent<Toggle>());
        m_ColorProgressDict.Add(colornum, trn.GetComponent<ProgressController>());
    }

    private void Awake()
    {
        Transform shapeArea = transform.FindChild("ShapeArea");
        if (shapeArea)
        {
            setShapeComponents(AppDefine.eShapeNum.TRIANGLE, shapeArea.FindChild("Triangle"));
            setShapeComponents(AppDefine.eShapeNum.RECTANGLE, shapeArea.FindChild("Rectangle"));
            setShapeComponents(AppDefine.eShapeNum.HEXAGON, shapeArea.FindChild("Hexagon"));
            setShapeComponents(AppDefine.eShapeNum.OCTAGON, shapeArea.FindChild("Octagon"));
            setShapeComponents(AppDefine.eShapeNum.DECAGON, shapeArea.FindChild("Decagon"));
            setShapeComponents(AppDefine.eShapeNum.DODECAGON, shapeArea.FindChild("Dodecagon"));
        }
        Transform colorArea = transform.FindChild("ColorArea");
        if (colorArea)
        {
            setColorComponents(2, colorArea.FindChild("Color_2"));
            setColorComponents(3, colorArea.FindChild("Color_3"));
            setColorComponents(4, colorArea.FindChild("Color_4"));
            setColorComponents(5, colorArea.FindChild("Color_5"));
        }
        Transform random = transform.FindChild("RandamArea/Toggle");
        if( random != null) { m_Randomize = random.GetComponent<Toggle>(); }
    }
    
    public void Update()
    {
        if (!m_Activate)
        {
            if (!AppMain.Instance.mbSceneChangeWait)
            {
                if (SaveManager.Instance.configSave) {
                    ConfigSaveData.Prefarence preference = SaveManager.Instance.configSave.mPreferenceInfo;
                    setupParameter((AppDefine.eShapeNum)preference._lastSelectShape, preference._lastSelectColor, preference._lastRandomOn);
                }
                AppMain.Instance.requestBlackOut();
                m_Activate = true;
            }
        }
    }

    /// <summary>
    /// パラメータ初期化
    /// </summary>
    public void setupParameter(AppDefine.eShapeNum shapenum, int colornum, bool randomize)
    {
        m_ShapeNum = shapenum;
        m_ColorNum = colornum;
        m_Random = randomize;

        foreach (var toggle in m_ShapeToggleDict)
        {
            toggle.Value.isOn = false;
        }
        m_ShapeToggleDict[m_ShapeNum].isOn = true;

        foreach (var toggle in m_ColorToggleDict)
        {
            toggle.Value.isOn = false;
        }
        m_ColorToggleDict[m_ColorNum].isOn = true;


        if (m_Random)
        {
            m_Randomize.isOn = true;
        }
        else
        {
            m_Randomize.isOn = false;
        }

        setupProgressInfo();
        setColorProgress(m_ShapeNum);
    }

    /// <summary>
    /// プログレス情報をマッピング
    /// </summary>
    private void setupProgressInfo()
    {
        ConfigSaveData configsave = SaveManager.Instance.configSave;
        ConfigSaveData.StageProgress progress = null;
        for (int i = 0; i < (int)AppDefine.eShapeNum.MAX; i++)
        {
            ProgressInShape data = new ProgressInShape();
            data._progressDict = new Dictionary<int, ConfigSaveData.eStageProgress>();
            bool bNoClear = true;
            bool bCleared = true;
            for (int j = 2; j < AppDefine.COLOR_NUM_MAX+2; j++)
            {
                progress = null;
                configsave.getProgressSaveData(ref progress, AppDefine.getStageID(AppDefine.shapeLink[i], j));
                if (progress == null)
                {
                    Debug.LogErrorFormat("getProgressError : stageID ={0}", AppDefine.getStageID(AppDefine.shapeLink[i], j));
                    continue;
                }
                data._progressDict.Add(j, progress._progressNum);
                if (progress._progressNum != ConfigSaveData.eStageProgress.NEW)
                {
                    bNoClear = false;
                }
                if (progress._progressNum != ConfigSaveData.eStageProgress.CLEAR)
                {
                    bCleared = false;
                }
            }
            if( bNoClear) { data._shapeProgress = ConfigSaveData.eStageProgress.NEW; }
            else if(bCleared) { data._shapeProgress = ConfigSaveData.eStageProgress.CLEAR; }
            else { data._shapeProgress = ConfigSaveData.eStageProgress.PLAYED; }

            m_ProgressInfoDict.Add(AppDefine.shapeLink[i], data);
            // シェイプのProgressは切り替わらないのでここで制御切り替え
            m_ShapeProgressDict[AppDefine.shapeLink[i]].Progress = data._shapeProgress;
        }
    }

    /// <summary>
    /// 指定した形に色側のプログレス情報を更新する
    /// </summary>
    /// <param name="shapenum"></param>
    private void setColorProgress(AppDefine.eShapeNum shapenum)
    {
        for (int j = 2; j < AppDefine.COLOR_NUM_MAX+2; j++)
        {
            m_ColorProgressDict[j].Progress = m_ProgressInfoDict[shapenum]._progressDict[j];
        }                
    }

    /// <summary>
    /// 三角形を選択
    /// </summary>
    public void pushTriangle()
    {
        if (!m_Activate) return;
        if (m_ShapeToggleDict[AppDefine.eShapeNum.TRIANGLE].isOn)
        {
            m_ShapeNum = AppDefine.eShapeNum.TRIANGLE;
            setColorProgress(m_ShapeNum);
            AppMain.SE_SELECT();
        }
    }
    /// <summary>
    /// 四角形を選択
    /// </summary>
    public void pushRectangle()
    {
        if (!m_Activate) return;
        if (m_ShapeToggleDict[AppDefine.eShapeNum.RECTANGLE].isOn)
        {
            m_ShapeNum = AppDefine.eShapeNum.RECTANGLE;
            setColorProgress(m_ShapeNum);
            AppMain.SE_SELECT();
        }
    }
    /// <summary>
    /// 六角形を選択
    /// </summary>
    public void pushHexagon()
    {
        if (!m_Activate) return;
        if (m_ShapeToggleDict[AppDefine.eShapeNum.HEXAGON].isOn)
        {
            m_ShapeNum = AppDefine.eShapeNum.HEXAGON;
            setColorProgress(m_ShapeNum);
            AppMain.SE_SELECT();
        }
    }
    /// <summary>
    /// 八角形を選択
    /// </summary>
    public void pushOctagon()
    {
        if (!m_Activate) return;
        if (m_ShapeToggleDict[AppDefine.eShapeNum.OCTAGON].isOn)
        {
            m_ShapeNum = AppDefine.eShapeNum.OCTAGON;
            setColorProgress(m_ShapeNum);
            AppMain.SE_SELECT();
        }
    }
    /// <summary>
    /// 十角形を選択
    /// </summary>
    public void pushDecagon()
    {
        if (!m_Activate) return;
        if (m_ShapeToggleDict[AppDefine.eShapeNum.DECAGON].isOn)
        {
            m_ShapeNum = AppDefine.eShapeNum.DECAGON;
            setColorProgress(m_ShapeNum);
            AppMain.SE_SELECT();
        }
    }
    /// <summary>
    /// 十二角形を選択
    /// </summary>
    public void pushDodecagon()
    {
        if (!m_Activate) return;
        if (m_ShapeToggleDict[AppDefine.eShapeNum.DODECAGON].isOn)
        {
            m_ShapeNum = AppDefine.eShapeNum.DODECAGON;
            setColorProgress(m_ShapeNum);
            AppMain.SE_SELECT();
        }
    }
    /// <summary>
    /// 二色を選択
    /// </summary>
    public void pushTwoColor()
    {
        if (!m_Activate) return;
        if (m_ColorToggleDict[2].isOn)
        {
            m_ColorNum = 2;
            AppMain.SE_SELECT();
        }
    }
    /// <summary>
    /// 三色を選択
    /// </summary>
    public void pushThreeColor()
    {
        if (!m_Activate) return;
        if (m_ColorToggleDict[3].isOn)
        {
            m_ColorNum = 3;
            AppMain.SE_SELECT();
        }
    }
    /// <summary>
    /// 四色を選択
    /// </summary>
    public void pushFourColor()
    {
        if (!m_Activate) return;
        if (m_ColorToggleDict[4].isOn)
        {
            m_ColorNum = 4;
            AppMain.SE_SELECT();
        }
    }
    /// <summary>
    /// 五色を選択
    /// </summary>
    public void pushFiveColor()
    {
        if (!m_Activate) return;
        if (m_ColorToggleDict[5].isOn)
        {
            m_ColorNum = 5;
            AppMain.SE_SELECT();
        }
    }

    /// <summary>
    /// ランダム化
    /// </summary>
    public void pushRandomize()
    {
        if (!m_Activate) return;
        m_Random = m_Randomize.isOn;
        AppMain.SE_SELECT();
    }

    /// <summary>
    /// 決定ボタンを押した
    /// </summary>
    public void pushSubmit()
    {
        if (!m_Activate) return;
        if (AppMain.Instance)
        {
            AppMain.Instance.requestBlackIn();
            AppMain.Instance.stageInfo = new AppMain.StageInfo_T(m_ShapeNum, m_ColorNum, m_Random);

            if(AppMain.Instance.mRandomColorShape != null) AppMain.Instance.mRandomColorShape.clearDataInfo(); // ステージセレクトからの遷移時はランダム情報を初期化
            // 最後に選択したものを保存
            SaveManager.Instance.configSave.mPreferenceInfo._lastSelectShape = (int)m_ShapeNum;
            SaveManager.Instance.configSave.mPreferenceInfo._lastSelectColor = m_ColorNum;
            SaveManager.Instance.configSave.mPreferenceInfo._lastRandomOn = m_Random;
            SaveManager.Instance.SaveAll();
            AppMain.Instance.mSystemStep = AppMain.eSystemStep.MENU_END;
        }
        AppMain.SE_OK();
    }
}
