﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using YFW;

/// <summary>
/// クリア時プレハブ制御
/// </summary>
public class ClearPrefabController : MonoBehaviour
{
    private GameObject mClearMask;           // クリアマスクオブジェクト
    private Text mResultRotate;              // リザルトスコア
    private Text mResultChange;              // リザルトスコア
    private Text mResultTime;                // リザルトタイム

    private GameObject mTimeHiscore;
    private GameObject mRotateHiscore;
    private GameObject mChangeHiscore;
    
    private void Awake()
    {
        mClearMask = transform.FindChild("ClearMask").gameObject;
        mResultTime = mClearMask.transform.FindChild("Time/TimeCount").GetComponent<Text>();
        mResultRotate = mClearMask.transform.FindChild("Rotate/RotateCount").GetComponent<Text>();
        mResultChange = mClearMask.transform.FindChild("Change/ChangeCount").GetComponent<Text>();

        mTimeHiscore = mClearMask.transform.FindChild("Time/TimeHiScore").gameObject;
        mRotateHiscore = mClearMask.transform.FindChild("Rotate/RotateHiScore").gameObject;
        mChangeHiscore = mClearMask.transform.FindChild("Change/ChangeHiScore").gameObject;
    }

    private void Start()
    {
        // 相手に参照できるよう渡す
        GameSystem.Instance.mClearCont = this;
    }

    /// <summary>
    /// ゲーム選択情報を入力＆ハイスコアがあればセーブ
    /// </summary>
    public void initialize(int stageID, int timecount, int rotate, int change)
    {
        mResultTime.text = string.Format(AppDefine.TextParamFormat, Math.Min(timecount, AppDefine.maxDispNum));
        mResultRotate.text = string.Format(AppDefine.TextParamFormat, Math.Min(rotate, AppDefine.maxDispNum));
        mResultChange.text = string.Format(AppDefine.TextParamFormat, Math.Min(change, AppDefine.maxDispNum));

        mTimeHiscore.SetActive(false);
        mRotateHiscore.SetActive(false);
        mChangeHiscore.SetActive(false);

        ConfigSaveData.StageProgress progress = null;
        SaveManager.Instance.configSave.getProgressSaveData(ref progress, stageID);
        if (progress != null)
        {
            bool bUpdate = false;
            // 一つでも-1であれば未設定なのですべてハイスコア更新
            if (progress._best_time == -1)
            {
                mTimeHiscore.SetActive(true);
                mRotateHiscore.SetActive(true);
                mChangeHiscore.SetActive(true);
                progress._best_time = timecount;
                progress._best_rotateNum = rotate;
                progress._best_changeNum = change;
                bUpdate = true;
            }
            else
            {
                if( progress._best_time > timecount)
                {
                    mTimeHiscore.SetActive(true);
                    progress._best_time = timecount;
                    bUpdate = true;
                }
                if (progress._best_rotateNum > rotate)
                {
                    mRotateHiscore.SetActive(true);
                    progress._best_rotateNum = rotate;
                    bUpdate = true;
                }
                if (progress._best_changeNum > change)
                {
                    mChangeHiscore.SetActive(true);
                    progress._best_changeNum = change;
                    bUpdate = true;
                }
            }
            if( bUpdate)
            {
                progress._progressNum = ConfigSaveData.eStageProgress.CLEAR;
                SaveManager.Instance.SaveAll();
            }
        }

        AudioManager.Instance.StopBGM();
        AudioManager.Instance.PlayBGM("result");

        mClearMask.gameObject.SetActive(true);
    }

    /// <summary>
    /// Ready Prefabを起動
    /// </summary>
    public void resetClearPrefab()
    {
        mClearMask.gameObject.SetActive(false);
    }

    /// <summary>
    /// タイトルへ戻る
    /// </summary>
    public void PushGotoTitle()
    {
        GameSystem.Instance.PushGotoTitle();
    }

    /// <summary>
    /// もう一度プレイ
    /// </summary>
    public void PushGotoRetry()
    {
        AudioManager.Instance.StopBGM();
        AppMain.SE_OK();
        AudioManager.Instance.PlayBGM("Mastering_long");
        GameSystem.Instance.gotoRetry();
    }

}
