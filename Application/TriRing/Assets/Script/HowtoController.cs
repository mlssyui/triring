﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using YFW;

public class HowtoController : MonoBehaviour
{
    // メンバ変数定義
    Transform   mWindow;
    Animator    mAnimator;
    Image       mImage;
    bool        mbImageSetup = false;

    private void Awake()
    {
        // UI参照リンク構築
        mWindow = transform.FindChild("Window");
        if( mWindow)
        {
            var tr = mWindow.FindChild("Image");
            if (tr != null) mImage = tr.GetComponent<Image>();
        }
        mAnimator = transform.GetComponent<Animator>();
        gameObject.SetActive(true);
    }

    private void Start()
    {
        // 相手に参照できるよう渡す
        GameSystem.Instance.mHowtoCont = this;
    }

    /// <summary>
    /// ヘルプボタンが押された時のアクション
    /// </summary>
    public void PushHowtoButton()
    {
        if (OpenWindow() == true)
        {
            AppMain.SE_OK();
            if (GameSystem.Instance != null)
            {
                if (GameSystem.Instance.GameStep == GameSystem.eGameStep.Ingame)
                {
                    GameSystem.Instance.mIsGamePlay = false;
                }
            }
        }
    }

    /// <summary>
    /// チュートリアルウィンドウを開く
    /// </summary>
    /// <returns></returns>
    public bool OpenWindow()
    {
        if (mAnimator)
        {
            if (mbImageSetup == false)
            {
                if (LocalizeManager.Instance.tutorialSpr != null && mImage != null)
                {
                    mImage.sprite = LocalizeManager.Instance.tutorialSpr;
                    mbImageSetup = true;
                }
                else
                {
                    Debug.LogError("TutorialImage load error!!");
                    return false;
                }
            }
            if (mbImageSetup == true)
            {
                mAnimator.ResetTrigger("Close");
                mAnimator.SetTrigger("Open");
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// OKボタンが押された時のアクション
    /// </summary>
    public void PushOKButton()
    {
        if (mAnimator)
        {
            mAnimator.ResetTrigger("Open");
            mAnimator.SetTrigger("Close");
            AppMain.SE_OK();
            if (GameSystem.Instance.GameStep == GameSystem.eGameStep.Ingame)
            {
                StartCoroutine(_checkReturn());
            }
        }
    }

    /// <summary>
    /// 遷移待機(正しい時間としてタイマーを戻す)
    /// </summary>
    /// <param name="blackin"></param>
    /// <returns></returns>
    private IEnumerator _checkReturn()
    {
        var wait = new WaitForEndOfFrame();
        while (true)
        {
            var animState = mAnimator.GetCurrentAnimatorStateInfo(0);
            if (animState.shortNameHash == Animator.StringToHash("OpenIdle"))
            {
                break;
            }
            yield return wait;
        }

        // アプリケーションポーズ
        //AppMain.Pause(false);
        GameSystem.Instance.mIsGamePlay = true;
    }
}
