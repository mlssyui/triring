﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace YFW
{
    /// <summary>
    /// パーティクルエフェクト管理
    /// </summary>
    public class ParticleEffectController : MonoBehaviour
    {
        /// <summary>
        /// エフェクトの管理タイプ
        /// </summary>
        public enum eftType
        {
            INVALID = -1,
            ONESHOT,
            LOOP,
        }

        public eftType         _eftType = eftType.INVALID;
        ParticleSystem  _particleHandle = null;

        void Awake()
        {
            _particleHandle = GetComponent<ParticleSystem>();
        }

        void Update()
        {

        }

        /// <summary>
        /// パーティクルのセットアップ
        /// </summary>
        /// <param name="type"></param>
        public void setupParticle(eftType type)
        {
            _eftType = type;
            if (_particleHandle == null)
            {
                _particleHandle = GetComponent<ParticleSystem>();
            }
        }

        /// <summary>
        /// エフェクトのトリガーを引く
        /// </summary>
        /// <param name="trigger">trueでPlay / falseで停止</param>
        /// <returns></returns>
        public bool Trigger(bool trigger)
        {
            if(_particleHandle == null) { Debug.LogError("パーティクルシステムが見つかっていない！"); }
            if (trigger)
            {
                if( !gameObject.activeSelf) { gameObject.SetActive(true); }
                switch (_eftType)
                {
                    case eftType.ONESHOT:
                        _particleHandle.Play();
                        break;
                    case eftType.LOOP:
                        _particleHandle.Play();
                        break;
                }                
            }
            else
            {
                _particleHandle.Stop();
            }
            return true;
        }
    }
}
