﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using YFW;

namespace YFW
{
    public class SaveManager : SystemManager<SaveManager>
    {

        public ConfigSaveData configSave;

        /// <summary>
        /// 開始時にデータを読み込む
        /// </summary>
        public void Start()
        {
            Load<ConfigSaveData>(ref configSave);
        }

        /// <summary>
        /// 指定したセーブデータをロードする
        /// </summary>
        /// <typeparam name="T">SaveData<T>型</typeparam>
        /// <param name="savedata"></param>
        /// <returns></returns>
        public bool Load<T>(ref T savedata) where T : SaveData<T>
        {
            savedata = this.gameObject.AddComponent<T>();
            if (savedata)
            {
                savedata.Load();
                // ロードしたデータを適用
                savedata.ApplyDataLoaded();
                return true;
            }
            return false;
        }

        /// <summary>
        /// 任意のセーブデータをセーブする
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="save"></param>
        public void Save<T>(T savedata) where T : SaveData<T>
        {
            if (savedata)
            {
                savedata.Save();
            }
        }

        public void SaveAll()
        {
            Save<ConfigSaveData>(configSave);
        }
    }
}
