﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;

namespace YFW
{
    /// <summary>
    /// AdMobマネージャー
    /// </summary>
    public class AdMobManager : SystemManager<AdMobManager>
    {
        public string Android_Banner;
        public string Android_Interstitial;
        public string ios_Banner;
        public string ios_Interstitial;

        private BannerView      m_BannerView;               // バナービューオブジェクト
        private bool            mb_HideBanner = false;      // バナーが非表示か？
        private InterstitialAd  m_Interstitial;                 // インタースティシャルオブジェクト
        private bool            mb_CloseInterstitial = false;   // インタースティシャルが閉じているか？
        //private AdRequest       request;

        public delegate void CallBack();

        private CallBack callback;

        private void Start()
        {
            RequestBanner();
        }

        /// <summary>
        /// バナーリクエスト
        /// </summary>
        private void RequestBanner()
        {
#if UNITY_ANDROID
            string adUnitId = Android_Banner;
#elif UNITY_IOS
            string adUnitId = ios_Banner;
#else
            string adUnitId = "unexpected_platform";
#endif

            // AdSize(320, 50)のバナー広告を生成
            m_BannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);
            AdRequest request = new AdRequest.Builder()
                    //.AddTestDevice(AdRequest.TestDeviceSimulator)   //テストデバイス時はコメントアウト
                    //.AddTestDevice("977264FEAF620AA52BF9D58996E6B4F0")
                    .Build();

            m_BannerView.LoadAd(request);
        }

        /// <summary>
        /// バナーを表示
        /// </summary>
        public void ShowBanner()
        {
            if (mb_HideBanner == true)
            {
                m_BannerView.Show();
                mb_HideBanner = false;
            }
        }

        /// <summary>
        /// バナーを非表示
        /// </summary>
        public void HideBanner()
        {
            if (mb_HideBanner == false)
            {
                m_BannerView.Hide();
                mb_HideBanner = true;
            }
        }


        /// <summary>
        /// インタースティシャルリクエスト
        /// </summary>
        private void RequestInterstitial()
        {
#if UNITY_ANDROID
            string adUnitId = Android_Interstitial;
#elif UNITY_IOS
            string adUnitId = ios_Interstitial;
#else
            string adUnitId = "unexpected_platform";
#endif
            // 使い捨て処理
            if (mb_CloseInterstitial == true)
            {
                m_Interstitial.Destroy();
            }

            // インタースティシャルを初期化
            m_Interstitial = new InterstitialAd(adUnitId);
            AdRequest request = new AdRequest.Builder()
                    //.AddTestDevice(AdRequest.TestDeviceSimulator)   //テストデバイス時はコメントアウト
                    //.AddTestDevice("977264FEAF620AA52BF9D58996E6B4F0")
                    .Build();

            m_Interstitial.LoadAd(request);
            m_Interstitial.OnAdClosed += HandleAdClosed;
            m_Interstitial.OnAdFailedToLoad += HandleAdReLoad;
        }


        /// <summary>
        /// インタースティシャル広告を閉じた時に走る
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void HandleAdClosed(object sender, System.EventArgs e)
        {
            mb_CloseInterstitial = true;

            RequestInterstitial();
        }

        /// <summary>
        /// 広告のロードに失敗したときに走る
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void HandleAdReLoad(object sender, System.EventArgs e)
        {
            mb_CloseInterstitial = true;

            StartCoroutine(_waitConnect());
        }

        // 次のロードまで30秒待つ
        IEnumerator _waitConnect()
        {
            while (true)
            {
                yield return new WaitForSeconds(30.0f);

                // ネットに接続できるときだけリロード
                if (Application.internetReachability != NetworkReachability.NotReachable)
                {
                    RequestInterstitial();
                    break;
                }
            }
        }

        /// <summary>
        /// インタースティシャル広告を呼び出し
        /// </summary>
        /// <param name="cb"></param>
        public void DisplayInterstitial(CallBack cb = null)
        {
            callback = cb;

            if (m_Interstitial.IsLoaded())
            {
                m_Interstitial.Show();
#if !UNITY_IOS || UNITY_EDITOR
                if (callback != null)
                {
                    callback();
                }
#endif
            }
            else
            {
                if (callback != null)
                {
                    callback();
                }
            }
        }
    }
}
