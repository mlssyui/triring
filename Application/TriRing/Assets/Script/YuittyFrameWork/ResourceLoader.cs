﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace YFW
{
    public class ResourceLoader : SystemManager<ResourceLoader>
    {
        /// <summary>
        /// 非同期読み込み
        /// </summary>
        /// <param name="path"></param>
        /// <param name="callback"></param>
        /// <returns></returns>
        public ResourceRequest loadAsync(string path, Action<UnityEngine.Object> callback = null)
        {
#if false
            var handle = Resources.LoadAsync<GameObject>(path);
            StartCoroutine(_loadAsync(handle, callback));
            return handle;
#else
            var obj = Resources.Load(path);
            callback(obj);
            return null;
#endif
        }
        
        private IEnumerator _loadAsync(ResourceRequest handle, Action<ResourceRequest> callback = null)
        {
            var wait = new WaitForEndOfFrame();
            while (!handle.isDone)
            {
                yield return wait;
            }
            if( callback != null)
            {
                callback(handle);
            }
            yield return null;
        }
    }
}
