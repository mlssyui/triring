﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IngameMenuController : MonoBehaviour
{
    Animator mAnimator;
    private void Awake()
    {
        mAnimator = GetComponent<Animator>();
    }

    /// <summary>
    /// メニュー起動
    /// </summary>
    public void pushMenuButton()
    {
        if (mAnimator)
        {
            // アプリケーションポーズ
            //AppMain.Pause(true);
            GameSystem.Instance.mIsGamePlay = false;
            mAnimator.ResetTrigger("Close");
            mAnimator.SetTrigger("Open");
            AppMain.SE_OK();
        }

    }

    /// <summary>
    /// りとらいを選択
    /// </summary>
    public void pushRetry()
    {
        if (mAnimator)
        {
            AppMain.SE_OK();
            mAnimator.SetTrigger("OpenRetry");
        }
    }

    /// <summary>
    /// りとらいではいを選択
    /// </summary>
    public void RetryYes()
    {
        AppMain.SE_OK();
        mAnimator.SetTrigger("CloseRetry");
        mAnimator.ResetTrigger("Open");
        mAnimator.SetTrigger("Close");
        GameSystem.Instance.gotoRetry();
    }

    /// <summary>
    /// りとらいでいいえを選択
    /// </summary>
    public void RetryNo()
    {
        if (mAnimator)
        {
            mAnimator.SetTrigger("CloseRetry");
            AppMain.SE_CANCEL();
        }
    }

    /// <summary>
    /// あきらめる選択
    /// </summary>
    public void pushRetire()
    {
        // 本当にあきらめますか？
        if (mAnimator)
        {
            AppMain.SE_OK();
            mAnimator.SetTrigger("OpenRetire");
        }
    }

    /// <summary>
    /// あきらめるではいを選択
    /// </summary>
    public void RetireYes()
    {
        GameSystem.Instance.PushGotoTitle();
    }

    /// <summary>
    /// あきらめるでいいえを選択
    /// </summary>
    public void RetireNo()
    {
        if (mAnimator)
        {
            mAnimator.SetTrigger("CloseRetire");
            AppMain.SE_CANCEL();
        }
    }

    /// <summary>
    /// ゲームに戻る
    /// </summary>
    public void pushReturnGame()
    {
        if(mAnimator)
        {
            mAnimator.ResetTrigger("Open");
            mAnimator.SetTrigger("Close");
            StartCoroutine(_checkReturn());
            AppMain.SE_CANCEL();
        }
    }

    /// <summary>
    /// 遷移待機(正しい時間としてタイマーを戻す)
    /// </summary>
    /// <param name="blackin"></param>
    /// <returns></returns>
    private IEnumerator _checkReturn()
    {
        var wait = new WaitForEndOfFrame();
        while (true)
        {
            var animState = mAnimator.GetCurrentAnimatorStateInfo(0);
            if (animState.shortNameHash == Animator.StringToHash("OpenIdle"))
            {
                break;
            }
            yield return wait;
        }

        // アプリケーションポーズ
        //AppMain.Pause(false);
        GameSystem.Instance.mIsGamePlay = true;
    }

}
