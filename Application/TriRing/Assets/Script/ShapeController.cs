﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

/// <summary>
/// 三角形が連なった多角形コントローラ
/// </summary>
public class ShapeController : MonoBehaviour
{

    // 外部入力で記憶するリスト
    public float mDegreeValue;    // １回転に必要な角度(多角形の構成)
    public int mNumOfEdges;     // 角数
    public int mGroupId;        // 図形グループID

    /// <summary>
    /// 動的配列なのでリストで保持
    /// </summary>
    public List<TriangleController> mTriangleList = new List<TriangleController>();
    /// <summary>
    /// 三角形辞書 (mIndex, TriangleController)
    /// </summary>
    public Dictionary<int, TriangleController> mTriangleDict;
            
    private void Awake()
    {
    }

    private void Start()
    {
    }

    /// <summary>
    /// 子階層に含まれるTriangleControllersを拾ってリストに配置する。
    /// </summary>
    public void setupChildrenTriangles()
    {
        var trilist = transform.GetComponentsInChildren<TriangleController>();
        foreach( var tri in trilist )
        {
            int idx = -1;
            string stridx = tri.name.Replace("Tri_", "");
            if( int.TryParse(stridx, out idx) )
            {
                tri.setupTriangle(idx, mGroupId);
                mTriangleList.Add(tri);
            }
        }
        // 参照しやすいように辞書を作る
        mTriangleDict = mTriangleList.ToDictionary(triangle => triangle.mIndex);
    }

    /// <summary>
    /// リトライ時などリセット
    /// </summary>
    public void resetParameter()
    {
        // 回転値リセット
        transform.rotation = Quaternion.identity;

    }

    /// <summary>
    /// 図形回転処理を開始する
    /// </summary>
    public void ChangeRotateAction()
    {
        // 操作禁止時間でなければ許可する
        if (GameSystem.Instance.mIsDisableInput) return;

        // 操作禁止を設定して自身もアクションを行う
        GameSystem.Instance.mIsDisableInput = true;
        StartCoroutine(_changeRotate(true));
        GameSystem.Instance.addRotate();
        //GameSystem.Instance.mStageCont.allChangeRotate();

        // TODO : 切り替えSEコール.
        YFW.AudioManager.Instance.PlaySe("se_rotate_new");
    }

    public void allRotateCall()
    {
        StartCoroutine(_changeRotate(true));
    }
    
    /// <summary>
    /// 回転コルーチン（後にスライドにして左右切り替え可能にするか？）
    /// </summary>
    /// <param name="right">右回りならtrue</param>
    /// <returns></returns>
    private IEnumerator _changeRotate(bool right)
    {
        float oldDeg = transform.rotation.eulerAngles.z;
        float newDeg = oldDeg;
        if( right)
        {
            newDeg = oldDeg - mDegreeValue;
        }else
        {
            newDeg = oldDeg + mDegreeValue;
        }
        int timer = AppDefine.ChangeRotateTime;
        var wait = new WaitForEndOfFrame();
        while (timer > -1)
        {
            float t = (float)1 - ((float)timer / (float)AppDefine.ChangeRotateTime);
            transform.rotation = Quaternion.AngleAxis(Mathf.SmoothStep(oldDeg,newDeg,t), Vector3.forward);
            timer--;
            yield return wait;  // 1f待機
        }

        trianglesIndexChange(!right);

        // 操作可能状態許可。
        GameSystem.Instance.mIsDisableInput = false;
        yield return null;
    }

    /// <summary>
    /// 子の三角形のインデックスを加算する
    /// </summary>
    /// <param name="increment"></param>
    void trianglesIndexChange( bool increment )
    {
        int maxindex = mTriangleList.Count;
        // 子のインデックスを変化させる.
        foreach (var tri in mTriangleList)
        {
            if (increment)
            {
                tri.mIndex++;
                if (tri.mIndex >= maxindex) { tri.mIndex = 0; }
            }
            else
            {
                tri.mIndex--;
                if (tri.mIndex < 0) { tri.mIndex = maxindex - 1; }
            }
        }
        // 辞書を再生成
        mTriangleDict = mTriangleList.ToDictionary(triangle => triangle.mIndex);
    }

    /// <summary>
    /// 指定した三角形を取得
    /// </summary>
    /// <param name="index">取得したい三角形Index</param>
    /// <returns></returns>
    public TriangleController getTriangle(int index)
    {
        if( mTriangleDict.ContainsKey(index))
        {
            return mTriangleDict[index];
        }
        return null;
    }

    /// <summary>
    /// 直下の色がすべてそろったときにtrue
    /// </summary>
    /// <returns></returns>
    public bool isColorComplete()
    {
        if (mTriangleList.Count == 0) return false; // 例外チェック

        // 全一致なので１つ目と同じ色かチェックする
        AppDefine.eColorId tmpColorId = mTriangleList[0].mColorId;
        for( int i=1; i<mTriangleList.Count; i++)
        {
            if( mTriangleList[i].mColorId != tmpColorId) { return false; }  // 不一致なのでfalse
        }
        // 全て問題なければtrue
        return true;
    }
}
