﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

/// <summary>
/// ランダムカラーシェイプステージを生成する
/// </summary>
public class GenRandomColorShape
{
    /// <summary>
    /// シェイプ管理構造体
    /// </summary>
    public class ShapePackage
    {
        public AppDefine.eColorId[] mColorIds;
        public int mCurrent;  // 現在位置 
        public int mMax;      // 最大個数
        public bool isFirstSetColored { get { return (mCurrent > 0); } }
        public bool isFullSetColored { get { return (mCurrent == mMax); } }
        public bool isReachColor { get { return (mCurrent == (mMax - 1)); } }

        public void setup(int maxColorNum)
        {
            mColorIds = new AppDefine.eColorId[maxColorNum];
            mCurrent = 0;
            mMax = maxColorNum;
        }

        /// <summary>
        /// 色を一つ入れる
        /// </summary>
        /// <param name="colorId">入れる色を指定</param>
        /// <returns>代入できなければfalse</returns>
        public bool pushColor(AppDefine.eColorId colorId)
        {
            if (isFullSetColored) return false;
            mColorIds[mCurrent] = colorId;
            mCurrent++;
            return true;
        }

        /// <summary>
        /// 色を並べ替える
        /// </summary>
        public void shuffleColor()
        {
            if (isFullSetColored == false) return;  // 全て入っているときのみ行う。
            // ランダムソート
            mColorIds = mColorIds.OrderBy(i => Guid.NewGuid()).ToArray();
        }
    }


    /// <summary>
    /// 乱数時に同じパターンを読み込むため記録
    /// </summary>
    /// 

    public List<ShapePackage> mShapeList = new List<ShapePackage>();

    private bool mbRandomizeGenereted = false;  // 任意の形と色の組み合わせのステージ生成済
    private int mShapeNum = 0;
    private int mColorNum = 0; 

    /// <summary>
    /// 乱数生成済記録(scene破棄で破棄)
    /// </summary>
    public bool checkRandomizeGenereted( int shapeNum, int colorNum)
    {
        if (mbRandomizeGenereted == false) return false;
        if (mShapeNum != shapeNum) return false;
        if (mColorNum != colorNum) return false;

        return true;
    }

    /// <summary>
    /// 初期化処理
    /// </summary>
    public void clearDataInfo()
    {
        mShapeList.Clear();
        mbRandomizeGenereted = false;
        mShapeNum = 0;
        mColorNum = 0;
    }

    public void genereteRandomStage(int shapeNum, int colorNum)
    {
        // 数字が不一致なら再生成
        if ((mShapeNum != shapeNum) || (mColorNum != colorNum)) { mbRandomizeGenereted = false; }
        if (mbRandomizeGenereted == true) return;

        clearDataInfo();
        mShapeNum = shapeNum;
        mColorNum = colorNum;

        // シェイプリストを作成.
        for (int i = 0; i < colorNum; i++)
        {
            ShapePackage shapePack = new ShapePackage();
            shapePack.setup((int)shapeNum);
            mShapeList.Add(shapePack);
        }

        AppDefine.eColorId nowColorId = AppDefine.eColorId.RED;

        // 順番に色を入れる.
        for (int i = 0; i < colorNum; i++)
        {
            for( int j=0; j < shapeNum; j++)
            {
                // 全てのシェイプに色が１つ以上入っていないか？
                bool notAllShapeSet = mShapeList.Any(t => t.isFirstSetColored == false);
                // 代入可能なリストを取得（すべて色入っているものは除外、全てのシェイプに色が入っていない場合は色リーチのものも除外）
                var tmpList = mShapeList.Where(t => (t.isFullSetColored == false) && ((notAllShapeSet == true) ?(t.isReachColor == false):true));
                // リスト最大値から乱数を取得
                int cur = UnityEngine.Random.Range(0, tmpList.Count());
                // 任意のindexのアイテムを取得
                var content = tmpList.Where( (value, index) => index == cur).FirstOrDefault();
                if( content != null)
                {
                    content.pushColor(nowColorId);
                }
                else
                {
                    Debug.LogError("checkShape Error!!");
                }
            }
            nowColorId++;
        }

        // 整合性チェック
        foreach (var shape in mShapeList)
        {
            if (shape.isFullSetColored)
            {
                shape.shuffleColor();   // 整合性チェックとともに色を切り替える。
            }
            else
            {
                Debug.LogError("Shape Check Error!!");
            }
        }

        mbRandomizeGenereted = true;
    }
}
