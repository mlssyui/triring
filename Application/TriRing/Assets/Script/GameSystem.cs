﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using YFW;

/// <summary>
/// ゲームシステム統括
/// </summary>
public class GameSystem : SystemManager<GameSystem>
{
    /// <summary>
    /// ゲームステップ
    /// </summary>
    public enum eGameStep
    {
        None,       // なし
        Init,       // インゲーム起動
        Load,       // データロード中
        Startup,    // ゲーム開始待機画面
        Ready,      // レディ中
        Ingame,     // ゲーム中
        Gameend,    // ゲーム終了        
        Retry,      // リトライ要求中
    };

    /// <summary>
    /// ゲームステップの外部確認（読み取り専用）
    /// </summary>
    public eGameStep GameStep {  get { return mGameMainStep; } }
    private eGameStep mGameMainStep = eGameStep.None;       // ゲームステップ
    public eGameStep mNextGameMainStep = eGameStep.None;    // 次のゲームステップ予約

    // 管理系オブジェクト
    public GameObject               mGameCanvasGO = null;   // ゲームキャンバスオブジェクト
    public GameObject               mStageObject = null;    // ステージオブジェクト
    public StageController          mStageCont = null;  // ステージコントローラ
    public ReadyPrefabController    mReadyCont = null;  // 準備コントローラ
    public ClearPrefabController    mClearCont = null;  // クリアコントローラ
    public HowtoController          mHowtoCont = null;  // 遊び方

    //
    
    // 制御系プロパティ
    private bool        mbLoadStage = false;
    public bool         mIsDisableInput = false;    // 操作を禁止する
    public bool         mIsGamePlay = false;        // ゲーム起動中
    // 
    public float        mTimeCount = 0f;            // 経過時間
    private int         _timecountInt = 0;
    public int          mTimeCountInt               // 経過時間
    {
        set
        {
            _timecountInt = value;
            if (mTimeText) { mTimeText.text = string.Format(AppDefine.TextParamFormat, Math.Min(_timecountInt, AppDefine.maxDispNum)); }
        }
        get { return _timecountInt; }
    }

    private int         _rotateNum = 0;
    private int         mRotateNum                  // 回転数スコア
    {
        set {
            _rotateNum = value;
            if (mRotateText) { mRotateText.text = string.Format(AppDefine.TextParamFormat, Math.Min(_rotateNum, AppDefine.maxDispNum)); }
        }
        get { return _rotateNum; }
    }
    private int         _changeNum = 0;
    private int         mChangeNum                  // 交換数スコア
    {
        set {
            _changeNum = value;
            if (mChangeText) { mChangeText.text = string.Format(AppDefine.TextParamFormat, Math.Min(_changeNum, AppDefine.maxDispNum)); }
        }
        get { return _changeNum; }
    }

    public GameObject   mUIArea;                    // UIエリア参照
    private Text        mRotateText;                // スコア表示テキスト
    private Text        mChangeText;                // スコア表示テキスト
    private Text        mTimeText;                  // タイム表示テキスト

    
    private readonly string mStageFileNameFormat = "Stage/Stage_{0:00}_{1}_Panel";
    
    /// <summary>
    /// 起動開始時に動作
    /// </summary>
    private void Start()
    {
        mNextGameMainStep = eGameStep.Init;

        var tr = transform.FindChild("Canvas");
        if (tr) {
            mGameCanvasGO = tr.gameObject;
        }

        mUIArea = GameObject.Find("UIArea");
        if( mUIArea)
        {
            mRotateText = mUIArea.transform.FindChild("HeaderFrameCanvas/Panel/RotateDisp/RotateNum").GetComponent<Text>();
            mChangeText = mUIArea.transform.FindChild("HeaderFrameCanvas/Panel/ChangeDisp/ChangeNum").GetComponent<Text>();
            mTimeText = mUIArea.transform.FindChild("HeaderFrameCanvas/Panel/TimeDisp/TimeText").GetComponent<Text>();

        }
    }

    private void Update()
    {
        //if (AppMain.isPause) return;
        InitStep();
        UpdateStep();
    }

    private void OnDestroy()
    {
        if (mStageObject)
        {
            Destroy(mStageObject);
        }
    }

    /// <summary>
    /// ゲームステップが変更されるときの処理
    /// </summary>
    private void InitStep()
    {
        switch(mNextGameMainStep)
        {
            case eGameStep.Init:
                mTimeCount = 0f;    // リセット
                mTimeCountInt = 0;
                mRotateNum = 0;
                mChangeNum = 0;
                // アクションなし
                break;
            case eGameStep.Startup:
                // アクションなし
                break;
            case eGameStep.Ready:
                mTimeText.text = string.Format(AppDefine.TextParamFormat, 0);
                break;
            case eGameStep.Ingame:
                mIsGamePlay = true;
                break;
            default:
                break;
        }
        InitFinilize();
    }

    /// <summary>
    /// InitStep完了時の処理
    /// </summary>
    private void InitFinilize()
    {
        if (mNextGameMainStep != eGameStep.None)
        {
            mGameMainStep = mNextGameMainStep;
            mNextGameMainStep = eGameStep.None;
        }
    }

    /// <summary>
    /// ゲームステップの更新処理
    /// </summary>
    private void UpdateStep()
    {
        switch(mGameMainStep)
        {
            case eGameStep.Init:
                if (!mbLoadStage)
                {
                    // ステージのロード処理を行う.
                    loadStage();
                }
                else
                {
                    mNextGameMainStep = eGameStep.Load;
                }
                break;
            case eGameStep.Load:
                if ((mReadyCont != null) && (mStageCont != null) && mbLoadStage)
                {
                    mReadyCont.setParameter(mStageCont.m_ShapeNum, mStageCont.m_ColorNum);
                    mReadyCont.activateAction();
                    if (AppMain.instance) AppMain.instance.requestBlackOut();
                    mNextGameMainStep = eGameStep.Startup;
                }
                break;
            case eGameStep.Startup:
                // アクションなし
                break;
            case eGameStep.Ready:
                // アクションなし
                break;
            case eGameStep.Ingame:
                // スコア記録開始
                // タイマー開始
                updateTimer();
                ClearCheck();
                break;
            case eGameStep.Gameend:
                break;
            case eGameStep.Retry:
                if( AppMain.instance.mbFadeWait == false)
                {
                    GameSystem.Instance.mIsGamePlay = true;
                    mReadyCont.resetReadyPrefab();
                    mClearCont.resetClearPrefab();
                    if (AppMain.instance)
                    {
                        mStageCont.loadStageData((int)AppMain.Instance.stageInfo._shapeNum, AppMain.Instance.stageInfo._colorNum, AppMain.Instance.stageInfo._randomize);
                    }
                    else
                    {
                        mStageCont.loadStageData(6,3,false);
                    }
                    mNextGameMainStep = eGameStep.Init;
                    AppMain.instance.requestBlackOut();
                }
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// ステージの読み込み
    /// </summary>
    private void loadStage()
    {
        Action<UnityEngine.Object> callback = (handle) => {
            GameObject obj = handle as GameObject;
            if (obj)
            {
                mStageObject = Instantiate<GameObject>(obj);
                mStageObject.transform.SetParent(mGameCanvasGO.transform);
                var trans = mStageObject.GetComponent<RectTransform>();
                trans.localPosition = Vector3.zero;
                trans.sizeDelta = Vector2.zero;
                trans.localScale = Vector3.one;
            }
            mbLoadStage = true;
        };
        if (AppMain.instance)
        {
            int shapenum = (int)AppMain.instance.stageInfo._shapeNum;
            int colornum = AppMain.instance.stageInfo._colorNum;
            ResourceLoader.instance.loadAsync(string.Format(mStageFileNameFormat, shapenum, colornum), callback);
        }
        else
        {   // デバッグ用
            ResourceLoader.instance.loadAsync(string.Format(mStageFileNameFormat, 6, 3), callback);
        }
        mNextGameMainStep = eGameStep.Load;
    }

    /// <summary>
    /// 自身が色交換対象かどうかをチェックして、対象であれば対になる三角形を返却する
    /// </summary>
    /// <param name="requestTriangle"></param>
    /// <returns>自身が色交換対象でなければnullを返却する</returns>
    public TriangleController checkTriangleChangeTarget(TriangleController requestTriangle)
    {
        if( mStageCont !=null )
        {
            return mStageCont.getPairTriangle(requestTriangle);
        }else { Debug.LogErrorFormat("GameSystem (checkTriangleChangeTarget): mStageContが見つからない！"); }
        return null;
    }

    /// <summary>
    /// タイマー更新
    /// </summary>
    private void updateTimer()
    {
        if( mIsGamePlay)
        {
            mTimeCount += Time.deltaTime;
            int tmpcount = Mathf.FloorToInt(mTimeCount);
            if ( mTimeCountInt != tmpcount)
            {
                mTimeCountInt = tmpcount;
            }
        }
    }

    /// <summary>
    /// 回転加算
    /// </summary>
    public void addRotate()
    {
        mRotateNum++;
    }

    /// <summary>
    /// 交換加算
    /// </summary>
    public void addChange()
    {
        mChangeNum++;
    }
    
    /// <summary>
    /// クリアチェック
    /// </summary>
    private void ClearCheck()
    {
        if (mStageCont.isClearCheck() == true)
        {
            mIsGamePlay = false;
            mClearCont.initialize(
                AppDefine.getStageID(AppMain.Instance.stageInfo._shapeNum,AppMain.Instance.stageInfo._colorNum),
                mTimeCountInt,mRotateNum,mChangeNum
                );
            mNextGameMainStep = eGameStep.Gameend;
        }
    }

    /// <summary>
    /// タイトルへ戻る
    /// </summary>
    public void PushGotoTitle()
    {
        if (AppMain.Instance)
        {
            AudioManager.Instance.StopBGM();
            AppMain.SE_OK();
            AppMain.Instance.BackToTitle();
        }
    }

    /// <summary>
    /// リトライ処理
    /// </summary>
    public void gotoRetry()
    {
        mNextGameMainStep = eGameStep.Retry;
        AppMain.instance.requestBlackIn();
    }

    /// <summary>
    /// 初回チュートリアル対応
    /// </summary>
    public void openTutorial()
    {
        if( mHowtoCont != null)
        {
            mHowtoCont.OpenWindow();
            SaveManager.Instance.configSave.mPreferenceInfo._tutorialStep = (int)ConfigSaveData.eTutorialStep.PLAYED;
            SaveManager.Instance.SaveAll();
        }
    }
}
