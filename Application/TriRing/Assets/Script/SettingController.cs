﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using YFW;

public class SettingController : MonoBehaviour
{
    // メンバ変数定義
    Slider mBGMSlider;
    Slider mSESlider;
    Transform mBG;
    Animator mAnimator;
    ConfigSaveData.Prefarence pref;

    private void Awake()
    {
        // UI参照リンク構築
        mBG = transform.FindChild("BG");
        mBGMSlider = mBG.FindChild("BGMSlider").GetComponent<Slider>();
        mSESlider = mBG.FindChild("SESlider").GetComponent<Slider>();

        pref = SaveManager.Instance.configSave.mPreferenceInfo;
        mBGMSlider.value = pref._BGMVolume;
        mSESlider.value = pref._SEVolume;

        mAnimator = transform.GetComponent<Animator>();
        gameObject.SetActive(true);
    }

    /// <summary>
    /// BGMスライダーが変更されたときに呼び出す
    /// </summary>
    public void ChangedBGMSlider()
    {
        if (mBGMSlider)
        {
            YFW.AudioManager.Instance.BGMVolume = mBGMSlider.value;
            pref._BGMVolume = mBGMSlider.value;
        }
    }

    /// <summary>
    /// SEスライダーが変更されたときに呼び出す
    /// </summary>
    public void ChangedSESlider()
    {
        if (mSESlider)
        {
            YFW.AudioManager.Instance.SEVolume = mSESlider.value;
            pref._SEVolume = mSESlider.value;
        }
    }

    /// <summary>
    /// 設定ボタンが押された時のアクション
    /// </summary>
    public void PushMenuButton()
    {
        if (mAnimator)
        {
            mAnimator.ResetTrigger("Close");
            mAnimator.SetTrigger("Open");
            AppMain.SE_OK();
        }
    }

    /// <summary>
    /// 設定画面からOKボタンが押された時のアクション
    /// </summary>
    public void PushOKButton()
    {
        if (mAnimator)
        {
            SaveManager.Instance.configSave.mPreferenceInfo = pref;
            SaveManager.Instance.SaveAll();
            mAnimator.ResetTrigger("Open");
            mAnimator.SetTrigger("Close");
            AppMain.SE_OK();
        }
    }

#if false
    private Transform getChildTransform(string name, Transform parent)
    {
        Transform t = null;
        if (parent == null) return null;
        var vals = parent.GetComponentsInChildren<Transform>();
        if (vals != null)
        {
            foreach (var tmp in vals)
            {
                if (tmp.name.Equals(name))
                {
                    t = tmp;
                    break;
                }
            }
        }
        return t;
    }
#endif
}
