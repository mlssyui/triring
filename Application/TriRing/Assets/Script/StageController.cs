﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif // UNITY_EDITOR
using YFW;

/// <summary>
/// ステージコントローラ(ゲームモード構成)
/// </summary>
public class StageController : MonoBehaviour
{
    // 外部入力で記憶するリスト
    public int m_ShapeNum = 6;
    public int m_ColorNum = 3;

    /// <summary>
    /// 動的配列なのでリストで保持
    /// </summary>
    public List<ShapeController> mShapeList = new List<ShapeController>();
    /// <summary>
    /// 多角形辞書 (mGroupId , ShapeController)
    /// </summary>
    public Dictionary<int, ShapeController> mShapeDict;

    public StageData mStageData = null;

    /// <summary>
    /// 交換情報を記録
    /// </summary>
    public List<StageData.ChangeData> mPairList = new List<StageData.ChangeData>();

    /// <summary>
    /// エフェクトを記録
    /// </summary>
    public List<ParticleEffectController> mEftList = new List<ParticleEffectController>();

    /// <summary>
    /// セットアップリンク完了
    /// </summary>
    public bool mbSetupLink = false;



    private void Awake()
    {
    }

    private void Start()
    {
        // 相手に参照できるよう渡す
        GameSystem.Instance.mStageCont = this;
        // 子階層多角形をセットアップ
        setupChildrenShapes();

        // テスト用. 交換情報をセットアップ(最終的には外部データ化)
        //mPairList.Add(new StageData.ChangeData(0,4,1,1));
        //mPairList.Add(new StageData.ChangeData(1,3,2,0));

        if (AppMain.Instance)
        {
            AppMain.StageInfo_T stageinfo = AppMain.Instance.stageInfo;
            loadStageData((int)stageinfo._shapeNum, stageinfo._colorNum, stageinfo._randomize);
        }
        else
        {
            loadStageData(6, 3, false);
        }

        setupEffects();
    }

    /// <summary>
    /// 子階層に含まれるShapeControllerを拾ってリストに配置する。
    /// </summary>
    public void setupChildrenShapes()
    {
        var shapelist = transform.GetComponentsInChildren<ShapeController>();
        Array.Sort(shapelist, (a, b) => a.mGroupId - b.mGroupId);
        foreach (var shape in shapelist)
        {
            int idx = -1;
            string stridx = shape.name.Replace("RotateObject_", "");
            if (int.TryParse(stridx, out idx))
            {
                mShapeList.Add(shape);
                shape.setupChildrenTriangles();
            }
        }
        // 参照しやすいように辞書を作る
        mShapeDict = mShapeList.ToDictionary(shape => shape.mGroupId);
        mbSetupLink = true;
    }

    /// <summary>
    /// エフェクトリストセットアップ
    /// </summary>
    public void setupEffects()
    {
#if false
        var eftlist = transform.GetComponentsInChildren<ParticleEffectController>();
        foreach (var eft in eftlist)
        {
            mEftList.Add(eft);
        }
#else
        var objList = transform.GetComponentsInChildren<Image>();
        foreach( var obj in objList)
        {
            var eft = obj.transform.GetComponentInChildren<ParticleEffectController>(true);
            if ( eft != null ){
                mEftList.Add(eft);
                eft.setupParticle(ParticleEffectController.eftType.ONESHOT);
            }
        }
#endif
    }

    /// <summary>
    /// ステージデータJsonの読み込み
    /// </summary>
    public void loadStageData(int shapetype, int colornum, bool randomize)
    {
        if (mbSetupLink == false) { Debug.LogError("セットアップされていない"); return; }

        if (mStageData != null)
        {
            Destroy(mStageData);
        }

        mStageData = gameObject.AddComponent<StageData>();
        mStageData.Load(shapetype, colornum);

        m_ShapeNum = mStageData.shapetype;
        m_ColorNum = mStageData.shapenum;

        // ランダムモードで色の流し込み方を切り替える
        if (randomize)
        {
            if(AppMain.Instance.mRandomColorShape == null)
            {
                AppMain.Instance.mRandomColorShape = new GenRandomColorShape();
            }
            if(AppMain.Instance.mRandomColorShape.checkRandomizeGenereted(m_ShapeNum, m_ColorNum) == false)
            {
                AppMain.Instance.mRandomColorShape.genereteRandomStage(m_ShapeNum, m_ColorNum);
            }
            for (int i = 0; i < m_ColorNum; i++)
            {
                mShapeDict[i].mGroupId = mStageData.shapes[i].groupid;
                for (int j = 0; j < m_ShapeNum; j++)
                {
                    mShapeDict[i].mTriangleDict[j].setupTriangle(j, mShapeDict[i].mGroupId, AppMain.Instance.mRandomColorShape.mShapeList[i].mColorIds[j]);
                }
            }

        }
        else
        {
            if (AppMain.Instance.mRandomColorShape != null)
            {
                AppMain.Instance.mRandomColorShape.clearDataInfo();
            }

            for (int i = 0; i < m_ColorNum; i++)
            {
                mShapeDict[i].mGroupId = mStageData.shapes[i].groupid;
                for (int j = 0; j < m_ShapeNum; j++)
                {
                    mShapeDict[i].mTriangleDict[j].setupTriangle(j, mShapeDict[i].mGroupId, mStageData.shapes[i].trianglesColorId[j]);
                }
            }
        }
        mPairList.Clear();
        for(int i=0; i<mStageData.pairs.Length; i++)
        {
            mPairList.Add(mStageData.pairs[i]);
        }
    }
    
#if UNITY_EDITOR
    /// <summary>
    /// 子階層にあるShapeControllerを拾ってStageDataにインポートする
    /// </summary>
    public void importChildrenShapes(int shapenum, int colornum)
    {
        if (mStageData == null)
        {
            mStageData = new StageData();
        }
        mStageData.shapetype = shapenum;
        mStageData.shapenum = colornum;
        // シェイプ情報取得
        ShapeController[] shapelist = transform.GetComponentsInChildren<ShapeController>();
        if (shapelist.Length != colornum)
        {
            Debug.LogErrorFormat("インポート時のカラー数={0}とシェイプの数={1}が異なる！", colornum, shapelist.Length);
            return;
        }
        Array.Sort(shapelist, (a, b) => a.mGroupId - b.mGroupId);
        mStageData.shapes = new StageData.shapeData[shapelist.Length];
        // シェイプ情報をセットアップしつつ三角形情報を取得
        for ( int i=0; i< shapelist.Length; i++)
        {
            mStageData.shapes[i] = new StageData.shapeData();
            mStageData.shapes[i].groupid = shapelist[i].mGroupId;

            // 三角形色情報のセットアップ
            TriangleController[] trilist = shapelist[i].transform.GetComponentsInChildren<TriangleController>();
            if (shapelist.Length != colornum)
            {
                Debug.LogErrorFormat("インポート時の角度数={0}と三角形の数={1}がシェイプ{2}にて異なる！", shapenum, trilist.Length, i);
                return;
            }
            Array.Sort(trilist, (a, b) => a.mIndex - b.mIndex);
            mStageData.shapes[i].trianglesColorId = new AppDefine.eColorId[trilist.Length];
            for (int j = 0; j < trilist.Length; j++)
            {
                mStageData.shapes[i].trianglesColorId[j] = trilist[j].mColorId;
            }
        }

        mStageData.pairs = new StageData.ChangeData[mPairList.Count];
        for (int i = 0; i < mPairList.Count; i++)
        {
            mStageData.pairs[i] = mPairList[i];
        }
        mStageData.Save();
    }
#endif

    /// <summary>
    /// 交換で対になる三角形があるかチェックして取得する
    /// </summary>
    /// <param name="requestTriangle">要求元の三角形</param>
    /// <returns>対になる三角形情報を返却</returns>
    public TriangleController getPairTriangle( TriangleController requestTriangle)
    {
        TriangleController retVal = null;
        // 該当の三角形からペア情報が存在するかチェック.
        foreach( var pair in mPairList)
        {
            if( (pair._groupA == requestTriangle.mGroupId)&&(pair._indexA == requestTriangle.mIndex) )
            {   // Aと一致
                if (mShapeDict.ContainsKey(pair._groupB))
                {
                    ShapeController shape = mShapeDict[pair._groupB];
                    retVal = shape.getTriangle(pair._indexB);
                }
                else { Debug.LogErrorFormat("StageController (getPairTriangle): pairのB情報がshapeDictに登録されていない！"); }
                break;
            }
            else if((pair._groupB == requestTriangle.mGroupId) && (pair._indexB == requestTriangle.mIndex))
            {   // Bと一致
                if (mShapeDict.ContainsKey(pair._groupA))
                {
                    ShapeController shape = mShapeDict[pair._groupA];
                    retVal = shape.getTriangle(pair._indexA);
                }
                else { Debug.LogErrorFormat("StageController  (getPairTriangle): pairのA情報がshapeDictに登録されていない！"); }
                break;
            }
        }
        return retVal;
    }

    /// <summary>
    /// クリアチェック
    /// </summary>
    /// <returns>全ての多角形で色一致がとれたらtrueを返却</returns>
    public bool isClearCheck()
    {
        foreach( var shape in mShapeList)
        {
            if( shape.isColorComplete() == false) { return false; } // 不一致でfalse返却
        }
        return true;
    }

    /// <summary>
    /// チェンジボタン（テスト実装用）
    /// </summary>
    public void pushChange(int listid)
    {
#if false
        if (mShapeDict.ContainsKey(mPairList[listid]._groupA)&&mShapeDict.ContainsKey(mPairList[listid]._groupB))
        {
            TriangleController tri = mShapeDict[mPairList[listid]._groupA].getTriangle(mPairList[listid]._indexA);
            tri.ChangeColorAction();
        }
#else
        // 操作禁止時間でなければ許可する
        if (GameSystem.Instance.mIsDisableInput) return;

        for ( int i=0; i<mPairList.Count; i++)
        {
            TriangleController tri = mShapeDict[mPairList[i]._groupA].getTriangle(mPairList[i]._indexA);
            tri.ChangeColorAction();
        }
        GameSystem.Instance.addChange();
        GameSystem.Instance.mIsDisableInput = true;

        foreach (var eft in mEftList)
        {
            eft.Trigger(true);
        }

        // TODO : 切り替えSEコール.
        YFW.AudioManager.Instance.PlaySe("se_change_new");
#endif
    }

    /// <summary>
    /// 色全交換機能
    /// </summary>
    public void allChangeRotate()
    {
        foreach( var shape in mShapeList)
        {
            shape.allRotateCall();
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(StageController))]
    public class StageControllerEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            StageController controller = target as StageController;

            base.OnInspectorGUI();
            //controller.m_ShapeNum = EditorGUILayout.IntField("ShapeNum",controller.m_ShapeNum);
            //controller.m_ColorNum = EditorGUILayout.IntField("ColorNum",controller.m_ColorNum);

            if (GUILayout.Button("ステージ構成データを解析してJson出力"))
            {
                controller.importChildrenShapes(controller.m_ShapeNum, controller.m_ColorNum);
            }
        }
    }
#endif // UNITY_EDITOR
}
