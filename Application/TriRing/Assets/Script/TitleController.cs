﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// タイトルコントローラ
/// </summary>
public class TitleController : MonoBehaviour
{
    Button mStartButton;

    private void Awake()
    {
        mStartButton = transform.FindChild("Panel/Button").GetComponent<Button>();
    }

    private void Start()
    {
        if( mStartButton)
        {
            mStartButton.onClick.AddListener( PushStart );
        }
    }

    /// <summary>
    /// はじめるボタンを押した
    /// </summary>
    public void PushStart()
    {
        if( AppMain.Instance)
        {
            //
            AppMain.Instance.requestBlackIn();
            AppMain.Instance.mSystemStep = AppMain.eSystemStep.TITLE_END;
        }
        // TODO : 切り替えSEコール.
        AppMain.SE_OK();
    }
    
}
