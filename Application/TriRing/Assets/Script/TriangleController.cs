﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;
#endif // UNITY_EDITOR

/// <summary>
/// 三角形一枚のコントローラ
/// </summary>
public class TriangleController : MonoBehaviour
{

    public int mGroupId;    // 所属図形グループID
    public int mIndex;      // 所属図形インデックス(何番目か)
    public AppDefine.eColorId mColorId;    // 現在の色ID
    Image mImage;


    private void Awake()
    {
        mImage = gameObject.GetComponent<Image>();
    }

    /// <summary>
    /// 三角形情報の初期化
    /// </summary>
    /// <param name="index">この三角形のID</param>
    /// <param name="groupId">この三角形が所属する図形グループID</param>
    /// <param name="colorid">ランダムモードなら指定して初期化</param>
    public void setupTriangle(int index, int groupId, AppDefine.eColorId colorid = AppDefine.eColorId.INVALID)
    {
        mIndex = index;
        mGroupId = groupId;
        if( colorid != AppDefine.eColorId.INVALID)
        {
            mColorId = colorid;
        }
        mImage.color = AppDefine.ColorDict[mColorId];
    }

    /// <summary>
    /// 三角形をタップして切り替えを開始する
    /// </summary>
    public void ChangeColorAction()
    {
        // 操作禁止時間でなければ許可する
        //if (GameSystem.Instance.mIsDisableInput) return;
        // 自分が交換対象であればアクションする。
        // 交換対象であれば相手のTriangleControllerを返却される。
        TriangleController triangleTarget = GameSystem.Instance.checkTriangleChangeTarget(this);
        if (triangleTarget == null) return;

        // 対になる相手にアクションを行う
        triangleTarget.ChangeColorActionRecieve(mColorId);
        // 操作禁止を設定して自身もアクションを行う
        //GameSystem.Instance.mIsDisableInput = true;
        StartCoroutine(_changeColor(mColorId, triangleTarget.mColorId, true));

    }

    /// <summary>
    /// 切り替えホストから呼ばれる相手三角形
    /// </summary>
    public void ChangeColorActionRecieve(AppDefine.eColorId hostColorId)
    {
        // 操作禁止時間でなければ許可する
        //if (GameSystem.Instance.mIsDisableInput) return;

        // クライアント側からの呼び出し。
        StartCoroutine(_changeColor(mColorId, hostColorId, false));
    }


    /// <summary>
    /// 色変えコルーチン
    /// </summary>
    /// <param name="oldColorId">変化元の色</param>
    /// <param name="newColorId">変化先の色</param>
    /// <param name="isHost">ホスト呼び出しか？</param>
    /// <returns></returns>
    private IEnumerator _changeColor(AppDefine.eColorId oldColorId, AppDefine.eColorId newColorId, bool isHost)
    {
        Color oldColor, newColor;
        oldColor = AppDefine.ColorDict[oldColorId];
        newColor = AppDefine.ColorDict[newColorId];
        int timer = AppDefine.ChangeColorTime;
        var wait = new WaitForEndOfFrame();
        while (timer > -1)
        {
            float t = (float)1 - ((float)timer / (float)AppDefine.ChangeColorTime);
            mImage.color = new Color(Mathf.Lerp(oldColor.r, newColor.r, t), Mathf.Lerp(oldColor.g, newColor.g, t), Mathf.Lerp(oldColor.b, newColor.b, t));
            timer--;
            yield return wait;  // 1f待機
        }

        mColorId = newColorId;
        // 操作可能状態許可。
        if (isHost)
        {
            GameSystem.Instance.mIsDisableInput = false;
        }
        yield return null;
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(TriangleController))]
    public class TriangleControllerEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            TriangleController controller = target as TriangleController;

            base.OnInspectorGUI();
            if (GUILayout.Button("色情報をimageに反映"))
            {
                var image = controller.gameObject.GetComponent<Image>();
                if (image)
                {
                    image.color = AppDefine.ColorDict[controller.mColorId];
                }
            }
        }
    }
#endif // UNITY_EDITOR
}
