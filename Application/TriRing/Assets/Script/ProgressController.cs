﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using YFW;

/// <summary>
/// 子階層にあるアイコンセットを制御するコントローラ
/// </summary>
public class ProgressController : MonoBehaviour
{
    GameObject mNewIcon;
    GameObject mClearIcon;

    private ConfigSaveData.eStageProgress _progress = ConfigSaveData.eStageProgress.INVALID;

    /// <summary>
    /// プログレス制御
    /// </summary>
    public ConfigSaveData.eStageProgress Progress
    {
        set {
            if (!mNewIcon) return;
            if (!mClearIcon) return;

            if (value != _progress)
            {
                _progress = value;
                switch (_progress)
                {
                    case ConfigSaveData.eStageProgress.NEW:
                        mNewIcon.SetActive(true);
                        mClearIcon.SetActive(false);
                        break;
                    case ConfigSaveData.eStageProgress.CLEAR:
                        mNewIcon.SetActive(false);
                        mClearIcon.SetActive(true);
                        break;
                    default:
                        mNewIcon.SetActive(false);
                        mClearIcon.SetActive(false);
                        break;
                }
            }
        }
        get { return _progress; }
    }
    
    private void Awake()
    {
        var tr = transform.FindChild("new_icon");
        if (tr)
        {
            mNewIcon = tr.gameObject;
        }
        tr = transform.FindChild("clear_icon");
        if (tr)
        {
            mClearIcon = tr.gameObject;
        }
        Progress = ConfigSaveData.eStageProgress.NEW;
    }
    
}
