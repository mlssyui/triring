﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class AppDefine
{
    /// <summary>
    /// 多角形Index
    /// </summary>
    public enum eShapeNum
    {
        INVALID     = -1,
        TRIANGLE    = 3,
        RECTANGLE   = 4,
        HEXAGON     = 6,
        OCTAGON     = 8,
        DECAGON     = 10,
        DODECAGON   = 12,

        MAX = 6,
    };
    public static eShapeNum[] shapeLink = new eShapeNum[(int)eShapeNum.MAX] { eShapeNum.TRIANGLE, eShapeNum.RECTANGLE, eShapeNum.HEXAGON, eShapeNum.OCTAGON, eShapeNum.DECAGON, eShapeNum.DODECAGON };

    /// <summary>
    /// 色ID定義
    /// </summary>
    public enum eColorId
    {
        INVALID = -1,
        RED,
        BLUE,
        YELLOW,
        GREEN,
        SKY,

        MAX = 5
    };

    public static int COLOR_NUM_MAX = 4;

    public static readonly int StageID_MAX = (int)eShapeNum.MAX * COLOR_NUM_MAX;

    /// <summary>
    /// 色値対応表
    /// </summary>
    public static Dictionary<eColorId, Color> ColorDict = new Dictionary<eColorId, Color>()
    {
        { eColorId.RED,    new Color(1.0f, 0.1569f, 0f) },
        { eColorId.BLUE,   new Color(0f, 0.2549f, 1f) },
        { eColorId.YELLOW, new Color(1f, 0.9608f, 0f) },
        { eColorId.GREEN,  new Color(0.2078f,0.6314f,0.4196f) },
        { eColorId.SKY,    new Color(0.4f,0.8f,1f) },
    };

    public static readonly int ChangeColorTime = 15;     // 色が変わるのにかかる時間(f)
    public static readonly int ChangeRotateTime = 15;    // 回転にかかる時間(f)

    public static readonly int ReadyCountNum = 3;       // 開始カウントダウン時間
    public static readonly int maxDispNum = 9999;

    public static readonly string TextParamFormat = "{0:0000}";

    /// <summary>
    /// ステージＩＤ取得
    /// </summary>
    /// <param name="shapeNum"></param>
    /// <param name="colorNum"></param>
    /// <returns></returns>
    public static int getStageID(eShapeNum shapeNum, int colorNum)
    {
        return ((int)shapeNum * 10) + colorNum;
    }
    /// <summary>
    /// ステージＩＤ取得
    /// </summary>
    /// <param name="shapeNum"></param>
    /// <param name="colorNum"></param>
    /// <returns></returns>
    public static int getStageID(int shapeNum, int colorNum)
    {
        return (shapeNum * 10) + colorNum;
    }
}
